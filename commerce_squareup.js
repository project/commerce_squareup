(function ($) {

  // SquareUp settingspayment form .
  var sqSettings = Drupal.settings.squareUp;

  // Elements definitions.
  var $nonce = $("#" + sqSettings.nonce.id),
    $errors = $("#" + sqSettings.errors.id),
    $form = $("#commerce-checkout-form-review");

  // Delete old nonce, if any.
  $nonce.val('');

  // Initialize payment form.
  var paymentForm = new SqPaymentForm({
    applicationId: sqSettings.appID,
    inputClass: sqSettings.inputClass,
    inputStyles: sqSettings.inputStyles,
    cardNumber: {
      elementId: sqSettings.card_number.id,
      placeholder: sqSettings.card_number.placeholder
    },
    cvv: {
      elementId: sqSettings.cvv.id,
      placeholder: sqSettings.cvv.placeholder
    },
    expirationDate: {
      elementId: sqSettings.expiration_date.id,
      placeholder: sqSettings.expiration_date.placeholder
    },
    postalCode: {
      elementId: sqSettings.postal_code.id,
      placeholder: sqSettings.postal_code.placeholder
    },
    callbacks: {
      cardNonceResponseReceived: function (errors, nonce, cardData) {
        if (errors) {
          // handle errors
          errors.forEach(function (error) {
            $errors.append("<li>" + error.message + "</li>");
          });
          $form.find('.checkout-continue').removeAttr('disabled');
          $form.find('.checkout-processing').addClass('element-invisible');
        } else {
          // handle nonce
          $nonce.val(nonce);
          $form.submit();
        }
      },
      unsupportedBrowserDetected: function () {
        $("#" + sqSettings.errors.id).append("<li>" + Drupal.t('Sorry, your browser is not supported') + "</li>");
      }
    }
  });

  // Receive nonce on form submission.
  $form.submit(function (e) {
    if (!$nonce.val()) {
      paymentForm.requestCardNonce();
      return false;
    }
  });

}(jQuery));
