Square Payments
=======================

Provides Square (https://squareup.com) payment method for Drupal Commerce.

Installation
------------

1. Install module and its dependencies as usual

2. Create new commerce payment instance and select Square as payment method.

3. Open rule action form and configure the payment method.
